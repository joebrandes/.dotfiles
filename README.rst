**Overview**

* Contents:

  + 1 Dotfiles_

    + 1.1 `Links to Gnu Stow`_
    + 1.2 Projects_

      + 1.2.1 ZSH_
      + 1.2.2 VIM_
      + 1.2.3 Starship_
      + 1.2.4 FONTS_
      + 1.2.5 TMUX_
      + 1.2.6 ROFI_
      + 1.2.7 RANGER_
      + 1.2.8 XRESOURCES_
      + 1.2.9 XMONAD_


########
Dotfiles
########


This is my Dotfiles-Projekt with Stow Helper.

::


     ____        _    __ _ _                 _            ____
    |  _ \  ___ | |_ / _(_) | ___  ___      | | ___   ___| __ )
    | | | |/ _ \| __| |_| | |/ _ \/ __|  _  | |/ _ \ / _ \  _ \
    | |_| | (_) | |_|  _| | |  __/\__ \ | |_| | (_) |  __/ |_) |
    |____/ \___/ \__|_| |_|_|\___||___/  \___/ \___/ \___|____/


Status: Work in Progress - Development on **HP Spectre Notebook** on LinMint


Links to Gnu Stow
=================

*   `Gnu Stow <https://www.gnu.org/software/stow/manual/stow.html>`_

*   `Build a portable Env <https://jakewiesler.notion.site/Build-A-Portable-Development-Environment-43d698395f65498f825113b841ba22a6>`_

*   `Managing Dotfiles <https://www.jakewiesler.com/blog/managing-dotfiles>`_

*   `Youtube: Gnu Stow for Dotfiles <https://www.youtube.com/watch?v=FHuwzbpTTo0>`_


Projects
========

ZSH
---

Obviously you need ZSH installed

::

    ./zsh
    ├── antigen.zsh
    └── .zshrc

You can always get a newer version of antigen.zsh via

`curl -L git.io/antigen > ~/.dotfiles/zsh/antigen.zsh`

Obviously you change the Git-Repo with that action.

VIM
---


After first Start of VIM: `:PlugInstall`

::

    ./vim
    ├── .vim
    │   └── autoload
    │       ├── pathogen.vim
    │       └── plug.vim
    └── .vimrc

So there is also **Pathogen** included, but i prefer **VimPlug** these days.

When the Plugins get created we get a new **Plugged** Folder
`.dotfiles/vim/.vim/plugged` which is excluded via `.gitignore`

Starship
--------


Test of Starship Config with this `.dotfiles` Folder

::

    ./starship
    └── .config
        └── starship.toml

Don't forget to install the Starship.rs stuff:

`sh -c "$(curl -fsSL https://starship.rs/install.sh)"`

For Bash:

`eval "$(starship init bash)"`

For Zsh:

`eval "$(starship init zsh)"`

FONTS
-----

I want the **MesloLGS NF** Nerdfont - so:

::

    ./fonts
    └── .local
        └── share
            └── fonts
                ├── MesloLGS NF Bold Italic.ttf
                ├── MesloLGS NF Bold.ttf
                ├── MesloLGS NF Italic.ttf
                └── MesloLGS NF Regular.ttf

They should automatically available in the User-Enviroment.


TMUX
----

Is with TMUX-POWER but is without Plugin Tech - just static loading

::

    ./tmux
    ├── .tmux
    │   ├── plugins
    │   │   ├── tmux-open
    │   │   │   ├── CHANGELOG.md
    │   │   │   ├── .gitattributes
    │   │   │   ├── LICENSE.md
    │   │   │   ├── open.tmux
    │   │   │   ├── README.md
    │   │   │   ├── scripts
    │   │   │   └── video
    │   │   ├── tmux-power
    │   │   │   ├── README.md
    │   │   │   ├── tmux-power-ORIGINAL.tmux
    │   │   │   └── tmux-power.tmux
    │   │   └── tmux-sidebar
    │   │       ├── CHANGELOG.md
    │   │       ├── docs
    │   │       ├── .gitattributes
    │   │       ├── LICENSE.md
    │   │       ├── README.md
    │   │       ├── screenshot.gif
    │   │       ├── scripts
    │   │       └── sidebar.tmux
    │   └── sidebar
    │       └── directory_widths.txt
    └── .tmux.conf

Again: no use of Tmux-Power because of the resulting Git-Repos for
the Tmux-Plugins!

ROFI
----

My special Version of Rofi for central Startmanagement in Desktops or WM.

Because of `config.rasi` and Theme `./themes/dracula-alternate-joeb.rasi`
easily started via `rofi -show drun`.

::

    ./rofi
    └── .config
        └── rofi
            ├── config
            ├── config.rasi
            └── themes
                ├── dracula-alternate-joeb.rasi
                ├── dracula-official-theme.rasi
                ├── gruvbox-common.inc
                ├── gruvbox-common.inc.rasi
                ├── gruvbox-dark.rasi
                ├── solarized-alternate-joeb.rasi
                ├── solarized-alternate.rasi
                └── solarized.rasi

Reminder: this also needs the **MesloLGS NF** Nerdfont.

RANGER
------

My Terminal Favorite: IMG-Preview via Ueberzug!

::

    ./ranger
    └── .config
        └── ranger
            ├── commands_full.py
            ├── commands.py
            ├── plugins
            │   ├── __init__.py
            │   ├── __pycache__
            │   └── ranger_devicons
            ├── rc.conf
            ├── rifle.conf
            └── scope.sh

For Ueberzug: Install via npm (or firstly nvm)


XRESOURCES
----------

If you want to get into the heavy Terminal-Stuff with `xterm` or `urxvt`

::

    ./xresources
    └── .Xresources

Quite frankly: I am using full Cinnamon Desktop mostly with `gnome-terminal`,
so most of the styling is via the Gnome-Terminal Stylesheet.



XMONAD
------

Just for Starters with `xmonad.hs` - lots more to configure and Install: `xmobar`, ...
