#!/bin/bash
# Workspace desktop image switcher WDIS 1.0
# Script to change the desktop image when switching workspaces.
# Tested on Linux Mint 13 'cinnamon'
killall xprop 2>/dev/null

if [ -z "$1" ]
  then
   desktop_dir="/home/joeb/Bilder/wallpaper/set-for-9-ws/" # full path to images directory;
  else
   desktop_dir="/home/joeb/Bilder/wallpaper/$1/" # full path to images directory;
fi

desktop_img[0]="Bild1.jpg"
desktop_img[1]="Bild2.jpg"
desktop_img[2]="Bild3.jpg"
desktop_img[3]="Bild4.jpg"
desktop_img[4]="Bild5.jpg"
desktop_img[5]="Bild6.jpg"
desktop_img[6]="Bild7.jpg"
desktop_img[7]="Bild8.jpg"
desktop_img[8]="Bild9.jpg"

# add more images if using more workspaces

setdesktop() {
   gsettings set org.gnome.desktop.background picture-uri "file://$desktop_dir$1"
   }
xprop -root -spy _NET_CURRENT_DESKTOP | (
   while read -r; do
      desk=${REPLY:${#REPLY}-1:1}
      setdesktop ${desktop_img[$desk]}
      sleep 0.5
   done
)
