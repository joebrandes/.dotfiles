#!/bin/sh
#-----------------------------------
#         #               ######  
#         #  ####  ###### #     # 
#         # #    # #      #     # 
#         # #    # #####  ######  
#   #     # #    # #      #     # 
#   #     # #    # #      #     # 
#    #####   ####  ###### ######  
#-----------------------------------
# see Luke Smith: https://www.youtube.com/watch?v=8E8sUNHdzG8 
# the basic oneliner
#man -k . | dmenu -l 30 | awk '{ print $1 }' | xargs -r man -T pdf 2>/dev/null | zathura -

# in 2 parts: (because of Managament Probs with Xmonad Windows
# and conditions for ESC the Manpages Selection Process

# with dmenu
#MENU=$(man -k . | dmenu -l 30 | awk '{ print $1 }')

# with rofi
#MENU=$(man -k . | rofi -dmenu | awk '{ print $1 }')

# with manpages and multiple Chapters (see man 5 passwd)
MENU=$(man -k . | rofi -dmenu -columns 1 | awk '{print $2 " " $1}' | sed 's/[\(\)]//g')
# classic if condition:
# if [ "$MENU" ]; then
#     echo $MENU | xargs -r man -T pdf 2>/dev/null | zathura -
# fi
# or short and efficient
[ "$MENU" ] && echo $MENU | xargs -r man -T pdf 2>/dev/null | zathura -
 
