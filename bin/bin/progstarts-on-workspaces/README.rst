###################
Overview Workspaces
###################

The idea is to establish a set of Workspaces for my *daily Driver Environment*:

#. **Browser**

   Standard: Firefox

#. **Dateien**

   Linux: Nemo

   Windows: Explorer

#. **Konsolen**

   Linux: Gnome Terminal (gnome-terminal)

   Windows: Windows Terminal (wt)

#. **Coding**

   Linux: VSCodium

   Windows: VSCode

#. **VMS**

   Linux: VirtualBox bzw. KVM/qemu

   Windows: VirtualBox bzw. Hyper-V

#. **Office**

   Linux: OnlyOffic bzw. LibreOffice

   Windows: Office 365 bzw. OnlyOffice

#. **Mail**

   Linux: Evolution

   Windows: Outlook

#. **Graphics**

   Linux: Gimp

   Windows: Affinty

#. **System**

   Linux: Systemtools

   Windows: Systemtools


Obviously just Examples of what could be used as Software on the Workspaces.

Remark:

   There are Versions in Use with only 4 Workspaces.
   In seminars for Example!

