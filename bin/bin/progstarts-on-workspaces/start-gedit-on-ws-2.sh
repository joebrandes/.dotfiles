#!/usr/bin/env bash 

# Start program in background and capture its PID 
# =============
# Original Idea:
# =============
# https://forums.linuxmint.com/viewtopic.php?t=172189

gedit &>/dev/null & 
pid=$! 

# Find the window id of the window we want to move, or wait if not yet found 
while true; do 
    wid=$(wmctrl -lp | sed -nr "s/^(\w*)\>.*\<${pid}\>.*$/\1/p" | head -n 1) 
    # echo $wid 
    [[ -n $wid ]] && break 
    sleep 0.1 
done 

# Move the window to the second workspace 
xdotool set_desktop_for_window $wid 1 

 
