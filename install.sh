#!/usr/bin/env bash

# Stow stuff
stow zsh
stow vim
stow starship
stow xresources
stow tmux
stow ranger
stow rofi
stow fonts
stow bin
stow wallpaper

# maybe later
# stow xmonad

